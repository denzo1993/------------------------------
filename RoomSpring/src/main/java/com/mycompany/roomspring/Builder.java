/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspring;

import java.util.Set;
import com.mycompany.roomspringdynamic_log.Log;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 *
 * @author Денис
 */
public class Builder {
    
    public Log build() throws Exception {
        final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Reflections reflections = new Reflections(new ConfigurationBuilder().setUrls(
                ClasspathHelper.forClassLoader(contextClassLoader)));

        Set<Class<? extends Log>> types = reflections.getSubTypesOf(Log.class);
        System.out.println("types=" + types);
        for (Class<? extends Log> type : types) {

            Log newInstance = (Log) contextClassLoader.loadClass(type.getName()).getConstructor().newInstance();
            System.out.println("newInstance=" + newInstance.toString());
            return newInstance;
        }
        return null;
    }
    
}
