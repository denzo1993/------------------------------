/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elements;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import rooms.Room;

/**
 *
 * @author Денис
 */
public class TestLamp extends TestCase {
    
    private Room room;
    
    public TestLamp() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetLamp_ON() {
        room.getLamp().setLAMP_ON(true);
        assertTrue(room.getLamp().isLAMP_ON());
    }
}
