/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_tests;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import rooms.Room;
import rooms.elements.Heating;

/**
 *
 * @author Денис
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestBeans.xml"})
public class TestHeatingRoom {
    
    @Resource(name = "room1")
    Room room;
    
    @Resource(name = "heating1")
    Heating heating;
    
    @Test
    public void testHeating() {
        heating.switch_on();
        Assert.assertEquals(room.getHeating(), heating);
        Assert.assertTrue(room.getHeating().isActive());
    }
}
