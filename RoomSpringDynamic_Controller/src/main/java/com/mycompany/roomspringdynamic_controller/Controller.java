/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_controller;

import rooms.Room;
/**
 *
 * @author Денис
 */
public class Controller {
    
    private final Room room;
    
    public Controller(Room room) {
        this.room = room;
    }
    
    public void stateChangedHandler() {
        checkTemp();
        checkLighting();
        room.getLog().log("");
    }
    
    /**
     * Функция проверки температуры
     * @return false - если затронуто состояние комнаты.
     * true - если температура в рамках нормы.
     */
    public boolean checkTemp() {
        if(room.getTemp() >= room.getMin_temp() && room.getTemp() <= room.getMax_temp()) {
            room.getHeating().switch_off();
            room.getWindow().close();
            room.getDoor().close();
            room.getLog().log("Температура(" + room.getTemp() + ") - [OK]");
            return true;
        }
        room.getLog().log("Температура не в рамках нормы...(" + room.getTemp() + ")");
        if(room.getTemp() < room.getMin_temp()) {
            room.getHeating().switch_on();
        }
        if(room.getTemp() > room.getMax_temp()) {
            room.getWindow().open();
        }
        return false;
    }
    
    public boolean checkLighting() {
        room.getLog().log("Датчик света: " + String.valueOf(room.getNaturalLighting()));
        if(room.getNaturalLighting() >= room.getNorm_lighting() && room.getLamp().isLAMP_ON()) {
            room.getLamp().setLAMP_ON(false);
            room.getLog().log("Лампа выключена");
            return false;
        }
        
        if(room.getNaturalLighting() <= room.getMin_lighting() && !room.getLamp().isLAMP_ON()) {
            room.getLamp().setLAMP_ON(true);
            room.getLog().log("Лампа включена");
            return false;
        }
        
        room.getLog().log("Естественное освещение: ОК");
        return true;
    }
    
}