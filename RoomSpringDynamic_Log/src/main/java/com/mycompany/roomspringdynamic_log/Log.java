/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import rooms.ILog;
/**
 *
 * @author Денис
 */
public class Log implements ILog{
    
    private final String name;
    private PrintWriter out;
    
    public Log(String name) {
        this.name = name;
        try {
            out = new PrintWriter(new File(name + ".txt").getAbsoluteFile());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void log(String message) {
        System.out.println("[" + name + "]: " + message);
        out.println("[" + name + "]: " + message);
        out.flush();
    }
}
