/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_room.elements;

import java.util.Timer;
import java.util.TimerTask;
import com.mycompany.roomspringdynamic_room.rooms.Room;

/**
 *
 * @author Денис
 */
public class Door {
    
    private final Room room;
    private boolean DOOR_OPENED;
    private final Timer timerDoor;
    
    public Door(Room room) {
        this.room = room;
        timerDoor = new Timer();
//        open();
    }
    
    public void close() {
        if(isClosed()) return;
        room.getLog().log("Дверь закрыта");
        timerDoor.cancel();
        DOOR_OPENED = false;
    }
    
    public void open() {
        if(isOpened()) return;
        room.getLog().log("Дверь открыта");
        timerDoor.schedule(new TimerTask() {
            @Override
            public void run() {
                room.reduceTemp(0.05);
            }
        }, 0, room.getUpdateInterval());
        DOOR_OPENED = true;
    }
    
    public boolean isClosed() {
        return !DOOR_OPENED;
    }
    
    public boolean isOpened() {
        return DOOR_OPENED;
    }
}
