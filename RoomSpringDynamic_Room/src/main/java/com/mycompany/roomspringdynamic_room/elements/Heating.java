/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_room.elements;

import java.util.Timer;
import java.util.TimerTask;
import com.mycompany.roomspringdynamic_room.rooms.Room;

/**
 *
 * @author Денис
 */
public class Heating {
    
    private final Room room;
    private boolean HEATING_ON;
    private Timer timerHeat;
    
    public Heating(Room room) {
        this.room = room;
        timerHeat = new Timer();
    }
    
    public void switch_on() {
        if(HEATING_ON) return;
        room.getLog().log("Обогревание включено");
        timerHeat = new Timer();
        timerHeat.schedule(new TimerTask() {
            @Override
            public void run() {
                room.increaseTemp(0.07);
            }
        }, 0, room.getUpdateInterval());
        HEATING_ON = true;
    }
    
    public void switch_off() {
        if(!HEATING_ON) return;
        room.getLog().log("Обогревание выключено");
        timerHeat.cancel();
        HEATING_ON = false;
    }
    
    public boolean isActive() {
        return HEATING_ON;
    }
    
}