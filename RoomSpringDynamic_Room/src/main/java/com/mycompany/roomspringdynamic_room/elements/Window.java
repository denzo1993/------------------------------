/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_room.elements;

import java.util.Timer;
import java.util.TimerTask;
import com.mycompany.roomspringdynamic_room.rooms.Room;

/**
 *
 * @author Денис
 */
public class Window {
    
    private final Room room;
    private boolean WINDOW_OPENED;
    private final Timer timerWindow;
    
    public Window(Room room) {
        this.room = room;
        timerWindow = new Timer();
    }
    
    public void close() {
        if(isClosed()) return;
        room.getLog().log("Окно закрыто");
        timerWindow.cancel();
        WINDOW_OPENED = false;
    }
    
    public void open() {
        if(isOpened()) return;
        room.getLog().log("Окно открыто");
        timerWindow.schedule(new TimerTask() {
            @Override
            public void run() {
                room.reduceTemp(0.1);
            }
        }, 0, room.getUpdateInterval());
        WINDOW_OPENED = true;
    }
    
    public boolean isClosed() {
        return !WINDOW_OPENED;
    }
    
    public boolean isOpened() {
        return WINDOW_OPENED;
    }
    
}
