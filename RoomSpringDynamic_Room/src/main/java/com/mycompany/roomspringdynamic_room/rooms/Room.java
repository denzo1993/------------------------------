/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.roomspringdynamic_room.rooms;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import com.mycompany.roomspringdynamic_room.elements.*;

/**
 *
 * @author Денис
 */
public class Room {
    
    private static final int updateInterval = 1000;
    private double min_temp;
    private double max_temp;
    private double temp; // Текущая темпа
    
    private double min_lighting;
    private double norm_lighting;
    private double naturalLighting;

    private Timer lightingTimer;
    
    private Controller controller;

    private Lamp lamp;

    private Heating heating;
    private Door door;
    private Window window;
    private Log l;
    
    /**
     * В xml указано, что этот метод будет вызываться после создания объекта Room
     */
    public void afterConstructor() {
        l.log("Комната создана");
        roomStateChanged();
        lightingTimer = new Timer();
        lightingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                double value = (double) (new Random().nextInt()%5) / 100;
                naturalLighting += value;
                roomStateChanged();
            }
        }, 0, updateInterval);
    }
    
    /**
     * Метод увеличения темпы
     * @param value 
     */
    public void increaseTemp(double value) {
        temp += value;
        l.log("Температура увеличена на " + value);
        roomStateChanged();
    }
    
    /**
     * Метод уменьшения темпы
     * @param value 
     */
    public void reduceTemp(double value) {
        temp -= value;
        l.log("Температура уменьшена на " + value);
        roomStateChanged();
    }
    
    public void setNaturalLighting(double value) {
        naturalLighting = value;
    }
    
    public double getNaturalLighting() {
        return naturalLighting;
    }
    
    public void roomStateChanged() {
        controller.stateChangedHandler();
    }
    
    public Log getLog() {
        return l;
    }
    
    public void setHeating(Heating heating) {
        this.heating = heating;
    }
    
    public Heating getHeating() {
        return heating;
    }
    
    public Window getWindow() {
        return window;
    }
    
    public void setWindow(Window window) {
        this.window = window;
    }
    
    public Door getDoor() {
        return door;
    }
    
    public void setDoor(Door door) {
        this.door = door;
    }
    
    public double getMin_temp() {
        return min_temp;
    }

    public void setMin_temp(double min_temp) {
        this.min_temp = min_temp;
    }

    public double getMax_temp() {
        return max_temp;
    }

    public void setMax_temp(double max_temp) {
        this.max_temp = max_temp;
    }
    
    public int getUpdateInterval() {
        return updateInterval;
    }
    
    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    public Log getL() {
        return l;
    }

    public void setL(Log l) {
        this.l = l;
    }
    
    public void setTemp(double value) {
        temp = value;
    }
    
    public double getTemp() {
        return temp;
    }
    
    public double getMin_lighting() {
        return min_lighting;
    }

    public void setMin_lighting(double min_lighting) {
        this.min_lighting = min_lighting;
    }

    public double getNorm_lighting() {
        return norm_lighting;
    }

    public void setNorm_lighting(double norm_lighting) {
        this.norm_lighting = norm_lighting;
    }
    
    public Lamp getLamp() {
        return lamp;
    }

    public void setLamp(Lamp lamp) {
        this.lamp = lamp;
    }
}
